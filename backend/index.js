const express = require('express')
const app = express()
const cookieParser = require('cookie-parser')
const http = require('http').createServer(app)
const io = require('socket.io')(http)

let users = []

app.use(express.json())
app.use(cookieParser())
app.use('/static', express.static('public'))
app.set('view engine', 'pug')
app.set('views', './views')

app.get('/', function (req, res) {
    res.render('pages/index')
})

app.get('/chat', function (req, res) {
    res.render('pages/chat')
})

const io_nsp = io.of('/public')

io_nsp.on('connection', function (socket) {
    socket.on('connection alert', (connectedUser) => {
        users.push(connectedUser)
        io_nsp.emit('connection alert',
            {
                message: `${connectedUser} is connected`,
                users: users
            })
    })
    socket.on('typing', (nickname) => {
        io_nsp.emit('typing', { message: `${nickname} is typing`, from: nickname })
    })
    socket.on('chat', function (payload) {
        io_nsp.emit('chat', payload)
    })
    socket.on('disconnect', (payload) => {
        io_nsp.emit('disconnection alert', {})
    })
});

http.listen(3000, function () {
    console.log('Server running')
})