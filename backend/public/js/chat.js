// const socket = io('http://localhost:3000')
const socket = io('/public')
/* let messages = [] */

socket.on('connect', function () {
    if (Cookies.get('nickname')) {
        socket.emit('connection alert', Cookies.get('nickname'))
    }
})

socket.on('connection alert', function (payload) {
    const connectedList = document.getElementById('connectedList')
    connectedList.innerHTML = ""
    let connectedUsers = []
    connectedUsers = payload.users
        .filter((val, index) => payload.users.indexOf(val) === index && val !== Cookies.get('nickname'))

    console.log(connectedUsers)
    connectedUsers.forEach(user => {
        const userListItem = document.createElement('li')
        userListItem.innerText = user
        connectedList.appendChild(userListItem)
    });
})

socket.on('chat', function (msg) {
    const messageList = document.getElementById('messageList')
    const listElement = document.createElement('li')
    listElement.innerHTML = `<b class='nickname'>${msg.nickname === Cookies.get('nickname') ? 'Me' : msg.nickname}</b> : ${msg.message}`
    listElement.className = 'message'
    listElement.classList.add(msg.nickname === Cookies.get('nickname') ? "my-message" : "other-message")

    messageList.appendChild(listElement)
    /* messages = [...messages,msg]
    console.log(messages) */
})


const messageForm = document.getElementById('messageForm')
messageForm.addEventListener('submit', sendMessage)

const messageInput = document.getElementById('messageInput')
function sendMessage(e) {
    e.preventDefault();
    socket.emit('chat', { message: messageInput.value, nickname: Cookies.get('nickname') || 'Anonymous' })
    messageInput.value = ''
    return false
}

/****************************************************************** */
const typingText = document.getElementById('typingText')
messageInput.addEventListener('keyup', handleMessageTyping)

function handleMessageTyping() {
    socket.emit('typing', Cookies.get('nickname') || 'Anonymous')
}

socket.on('typing', (msg) => {
    typingText.innerText = msg.message
    if (msg.from !== Cookies.get('nickname')) {
        typingText.classList.add('show')
        typingText.classList.remove('hidden')
        setTimeout(() => {
            typingText.classList.add('hidden')
            typingText.classList.remove('show')
        }, 3000)
        clearTimeout()
    }
})