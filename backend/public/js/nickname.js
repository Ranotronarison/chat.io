const registerForm = document.getElementById('registerForm')
registerForm.addEventListener('submit', submitNickname)

document.getElementById('nickname').value = Cookies.get('nickname') || ''

function submitNickname (e) {
    e.preventDefault();
    const nickname = document.getElementById('nickname').value
    if(nickname){
        Cookies.set('nickname', nickname)
        window.location.href = '/chat'
    }
    return false
}